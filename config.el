;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Alaksandr Suša"
      user-mail-address "isushik94@gmail.com")

(defvar user-home-directory (concat (getenv "HOME") "/"))

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:

(setq doom-font (font-spec :family "Hack" :size 15)
      doom-variable-pitch-font (font-spec :family "Noto Sans" :size 16))

;(add-to-list 'default-frame-alist '(font . "Hack-13"))
;(setq doom-themes-treemacs-enable-variable-pitch nil)

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; Change rust default LSP server
; (after! rustic (setq rustic-lsp-server 'rust-analyzer))
; (after! rustic (setq rustic-lsp-server 'rls))

(setq racer-rust-src-path (concat user-home-directory ".rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/library"))

(setq-default tab-width 4)

;; Indent with spaces instead of tabs by default. Modes that really need tabs should enable 
;; indent-tabs-mode explicitly. Makefile-mode already does that, for example. 
;; If indent-tabs-mode is off, replace tabs with spaces before saving the file.
(setq-default indent-tabs-mode nil)
(add-hook 'write-file-hooks
  (lambda ()
    (if (not indent-tabs-mode)
      (untabify (point-min) (point-max))
    )
    nil
  )
)
;; When wrapping with the Emacs fill commands, wrap at 110 chars.
(setq-default fill-column 120)
(setq lsp-pyls-plugins-pycodestyle-max-line-length 120) 

;; whitespace
(setq whitespace-line-column 120)
(add-hook 'before-save-hook 'whitespace-cleanup)
;(global-whitespace-mode t)
;(add-hook 'prog-mode-hook 'whitespace-mode)
;(add-hook 'conf-mode-hook 'whitespace-mode)

;; cargo
(bind-keys :prefix-map cargo-mode-map
           :prefix "C-c c"
           ("C" . cargo-process-repeat)
           ("." . cargo-process-repeat)
           ("X" . cargo-run-example)
           ("b" . cargo-process-build)
           ("d" . cargo-process-doc)
           ("e" . cargo-process-bench)
           ("R" . cargo-process-current-test)
           ("f" . cargo-process-fmt)
           ("i" . cargo-process-init)
           ("n" . cargo-process-new)
           ("o" . cargo-process-current-file-tests)
           ("s" . cargo-process-search)
           ("u" . cargo-process-update)
           ("x" . cargo-process-run)
           ("t" . cargo-process-test)
           ("R" . cargo-process-test-regexp))

(add-hook 'toml-mode-hook 'cargo-minor-mode)
(setq company-minimum-prefix-length 3
      company-idle-delay 0)
;; rustic

; (setq lsp-rust-analyzer-cargo-watch-command (executable-find "clippy" exec-path))
(setq rustic-format-on-save t)
; (setq rustic-lsp-server 'rls)

;; evil mode -- change commands

(defun my/kill-this-buffer ()
  (interactive)
  (kill-buffer (current-buffer))
)

(defun my/write-and-kill-buffer ()
  (interactive)
  (save-buffer)
  (my/kill-this-buffer)
)

;; :q should kill the current buffer rather than quitting emacs entirely
(evil-ex-define-cmd "q" 'my/kill-this-buffer)

;; :wq should save the current buffer and kill it
(evil-ex-define-cmd "wq" 'my/write-and-kill-buffer)

;; Need to type out :quit to close emacs
(evil-ex-define-cmd "quit" 'evil-quit)

;; Maximaxing on start-up
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; may fix lsp freezes
;(setq lsp-enable-file-watchers nil)

(setq +python-ipython-repl-args '("-i" "--simple-prompt" "--no-color-info"))
(setq +python-jupyter-repl-args '("--simple-prompt"))

;(setq flycheck-check-syntax-automatically '(save mode-enable))
;; the default value was '(save idle-change new-line mode-enabled)

; disables insane electric indent mode (it makes annoying and wrong auto indentation during typing)
(electric-indent-mode -1)

(after! eglot
  :config
;  (set-eglot-client! 'cc-mode '("clangd" "-j=3" "--clang-tidy"))
;  (set-eglot-client! 'cc-mode '("ccls" "--init={\"index\": {\"threads\": 3}}"))
  (setq-hook! '(eglot--managed-mode-hook lsp-managed-mode-hook)  flycheck-disabled-checkers '(c/c++-clang c/c++-gcc))
)
; (add-hook 'c++-mode-hook (lambda () (setq flycheck-gcc-language-standard "c++17" flycheck-clang-language-standard "c++17")))

(setq show-paren-context-when-offscreen nil)

; ue.el
(after! ue
  :config
  (ue-global-mode +1)
  (define-key ue-mode-map (kbd "C-c u") 'ue-command-map)
)

(after! dap-mode
  (setq dap-python-debugger 'debugpy))

(after! centaur-tabs
  :config
  (centaur-tabs-group-by-projectile-project)
)

